
package Controller;

import Model.Matrix;
import java.util.Scanner;

/**
 *
 * @author anhnb
 */
public class Helpers {
    Scanner in;

    public Helpers() {
        in = new Scanner(System.in);
    }
    /**
     * 
     * @return là số sau khi parse String
     */
    public int checkInt(){
        String n = "";
        while(true){
        try {
            n = in.nextLine();
            return Integer.parseInt(n);
        } catch (Exception e) {
            System.out.println("Enter again ");
        }
        }
    }
    /**
     * 
     * @param m1
     * @param m2
     * @return true - nếu 2 ma trận có cùng số hàng và số cột
     *         false - nếu 2 ma trận không có cùng số hàng và số cột
     */
    
    public boolean checkAddSub(Matrix m1,Matrix m2){
        if( (m1.getRow()== m2.getRow()) //cung so hang
                && m1.getColumn() == m2.getColumn() ){ //cung so cot
            return true;
        }
        return false;
    }
    // mxn * nxp
    public boolean checkMultil(Matrix m1, Matrix m2){
        if( m1.getColumn() == m2.getRow() ){
            return true;
        }
        return false;
    }
    
}
