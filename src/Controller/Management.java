package Controller;

import Model.Matrix;
import java.util.ArrayList;
import java.util.Scanner;

public class Management {

    Matrix m1;
    Matrix m2;
    Scanner in;
    Helpers helpers;

    public Management() {
        in = new Scanner(System.in);
        helpers = new Helpers();
    }

    public void menu() {
        int option;
        while (true) {
            System.out.println("1. Addition Matrix");
            System.out.println("2. Subtraction Matrix");
            System.out.println("3. Multiplication Matrix");
            System.out.println("4. Quit");
            option = helpers.checkInt();
            switch (option) {
                case 1:
                    addition();
                    break;
                case 2:
                    subtraction();
                    break;
                case 3:
                    multil();
                    break;
                case 4:
                    return;
            }
        }
    }

    public Matrix input() {
        Matrix m = null;
        try {
            System.out.println("Enter row of matrix: ");
            int row = helpers.checkInt();
            System.out.println("Enter column of matrix: ");
            int column = helpers.checkInt();
            int value = 0;
            m = new Matrix(row, column);
            
            for (int i = 0; i < m.getRow(); i++) {
                for (int j = 0; j < m.getColumn(); j++) {
                    System.out.printf("Enter [%1d][%1d]: ", i, j);
                    value = helpers.checkInt();
                    m.setIndex(i, j, value);
                }
            }
           
        } catch (Exception e) {
            e.printStackTrace();
        }
         return m;
    }

    public void enterMatrix() {
        System.out.println("Enter matrix 1: ");
        m1 = input();
        System.out.println("Enter matrix 2: ");
        m2 = input();
    }

    public void addition() {
        try {
            enterMatrix();
            if (helpers.checkAddSub(m1, m2) == false) {
                System.out.println("wrong format matrix");
                return;
            }
            //add
            m1.display();
            System.out.println("+");
            m2.display();
            System.out.println("=");
            int value;
            Matrix m3 = new Matrix(m1.getRow(), m1.getColumn());
            for (int i = 0; i < m1.getRow(); i++) {
                for (int j = 0; j < m1.getColumn(); j++) {
                    value = m1.getIndex(i, j) + m2.getIndex(i, j);
                    m3.setIndex(i, j, value);
                }
            }
            System.out.println("---- Result ----");
            m3.display();
        } catch (Exception e) {
            System.out.println("addition err");
            e.printStackTrace();

        }

    }

    /**
     *
     */
    public void subtraction() {
        try {
            enterMatrix();
            if (helpers.checkAddSub(m1, m2) == false) {
                System.out.println("wrong format matrix");
                return;
            }
            //add
            m1.display();
            System.out.println("-");
            m2.display();
            System.out.println("=");
            int value;
            Matrix m3 = new Matrix(m1.getRow(), m1.getColumn());
            for (int i = 0; i < m1.getRow(); i++) {
                for (int j = 0; j < m1.getColumn(); j++) {
                    value = m1.getIndex(i, j) - m2.getIndex(i, j);
                    m3.setIndex(i, j, value);
                }
            }
            System.out.println("---- Result ----");
            m3.display();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    mxn * nxp = mxp
    2x3 * 3x4 = 2x4
    
     1 2 3 + 5 6 7 9 = 20 16 29  36 
     7 8 9   3 2 5 7   86 76 125  ... 
             3 2 4 4   
     */
    public void multil() {
        try {
            enterMatrix();
            if (!helpers.checkMultil(m1, m2)) {
                System.out.println("Wrong format matrix");
                return;
            }

            m1.display();
            System.out.println("+");
            m2.display();
            System.out.println("=");
            int value = 0;

            Matrix m3 = new Matrix(m1.getRow(), m2.getColumn());

            for (int i = 0; i < m1.getRow(); i++) {
                for (int j = 0; j < m2.getColumn(); j++) {
                    for (int k = 0; k < m1.getColumn(); k++) {
                        value += m1.getIndex(i, k) * m2.getIndex(k, j);
                    }
                    m3.setIndex(i, j, value);
                    value = 0;
                }
            }
            m3.display();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
