
package Model;


public class Matrix {
    int[][] matrix;
    //constructor
    public Matrix(int row,int column) {
        matrix = new int[row][column];
    }
    
    public int getRow(){
        return matrix.length;
    }

    public int getColumn(){
        return matrix[0].length;
    }
    //setIndex(2,3,5)
    public void setIndex(int row,int column,int value){
        matrix[row][column] = value;
    }
    
    public int getIndex(int row,int column){
        return matrix[row][column];
    }
    
    public void display(){
        for (int i = 0; i < this.getRow(); i++) {
            for (int j = 0; j < this.getColumn(); j++) {
                System.out.printf("[%1d]",this.getIndex(i, j));
            }
            System.out.println("");
        }

        
    }
    
}
